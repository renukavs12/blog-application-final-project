import { Component, OnInit } from '@angular/core';
import { User } from '../user';
import { UserService } from '../user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit
{
 user!:User[];

 constructor(private userService:UserService,private router:Router)
 {}

  ngOnInit(): void {
    this.getUser();
  }

  getUser()
  {
    this.userService.getUserList().subscribe(data=>{
      this.user=data;
    });
  }

  userDetails(id:number)
  {
    this.router.navigate(['user-details',id]);
  }

  updateUser(id:number)
  {
    this.router.navigate(['user-details',id]);
  }

  deleteUser(id:number)
  {
    this.userService.deleteUser(id).subscribe(data=>{
      console.log(data);
      this.getUser();
    })
  }


}