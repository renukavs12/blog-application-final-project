import { Component, OnDestroy, OnInit } from '@angular/core';
import { PostsService } from './posts.service';
import { post } from './post.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-postlist',
  templateUrl: './postlist.component.html',
  styleUrls: ['./postlist.component.scss']
})
export class PostlistComponent implements OnInit, OnDestroy{

  
  posts:post[]=[]
  private postSub: Subscription;
 constructor(public ps:PostsService) {}

 ngOnInit(): void {
   this.posts=this.ps.getAll();
   this.postSub=this.ps.getUpdateListner().subscribe((posts:post[])=>{
    this.posts=posts;
   })
    

   
 }
 ngOnDestroy(): void {
   this.postSub.unsubscribe();
 }

 editPost(post: any) {
  // Implement your logic to edit the post here
  console.log("Editing post:", post);
}


deletePost(data: any) {
  const index = this.posts.indexOf(data);
  if (index >= 0) {
    this.posts.splice(index, 1);
    console.log('Post has been permanently deleted.');
  }
}
}




