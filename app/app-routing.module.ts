import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './admin/home/home.component';
import { DashboardComponent } from './admin/dashboard/dashboard.component';
import { PostsComponent } from './admin/dashboard/posts/posts.component';

import { LoginComponent } from './login/login.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { SubscriptionComponent } from './admin/dashboard/subscription/subscription.component';
import { LogoutComponent } from './admin/dashboard/logout/logout.component';
import { UserListComponent } from './user-list/user-list.component';
import { CreateUserComponent } from './create-user/create-user.component';
import { UpdateUserComponent } from './update-user/update-user.component';
import { UserDetailsComponent } from './user-details/user-details.component';

const routes: Routes = [



  {
    path:'', component:LoginComponent
  },
  {
    path:'sign-up', component:SignUpComponent
  },
  {
    path:'',component:HomeComponent,
    children:[
      {
        path:"dashboard",component:DashboardComponent
      },
      {
        path:"posts",component:PostsComponent
      },
      {
        path:"subscription",component:SubscriptionComponent
      },
  
  
      {
        path:'',component:LogoutComponent
      }
    
    ]
    
  },
  {path:'users',component:UserListComponent},
  {path:'create-user',component:CreateUserComponent},
  {path:'',redirectTo:'users',pathMatch:"full"},
  {path:'update-user/:id',component:UpdateUserComponent},
  {path:'user-details/:id',component:UserDetailsComponent},
 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
